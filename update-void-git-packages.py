#!/usr/bin/env python3

import argparse
import subprocess
import sys

from pathlib import Path


class Pkg:
    def __init__(self, name, branch: str = 'master'):
        self.name = name
        self.branch = branch


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('voidpackages')
    opts = parser.parse_args()

    pkgs = [
        Pkg('emacs-company'),
        Pkg('emacs-compat', branch='main'),
        Pkg('emacs-dash'),
        Pkg('emacs-f'),
        Pkg('emacs-ht'),
        Pkg('emacs-hydra'),
        Pkg('emacs-kkp'),
        Pkg('emacs-libegit2', branch='main'),
        Pkg('emacs-lsp-mode'),
        Pkg('emacs-lsp-pyright'),
        Pkg('emacs-lsp-ui'),
        Pkg('emacs-magit', branch='main'),
        Pkg('emacs-markdown-mode'),
        Pkg('emacs-s'),
        Pkg('emacs-solarized'),
        Pkg('emacs-spinner'),
        Pkg('emacs-which-key'),
        Pkg('emacs-with-editor', branch='main'),
        Pkg('foot-pgo'),
        Pkg('river-git'),
    ]

    srcpkgs = Path(opts.voidpackages) / 'srcpkgs'

    for pkg in pkgs:
        template = srcpkgs / pkg.name / 'template'
        subprocess.run(['./update-void-template-version.py', opts.voidpackages, template, '--branch', pkg.branch], check=True)
    

if __name__ == '__main__':
    sys.exit(main())
