#!/usr/bin/env python3

import argparse
import sys
import tempfile
import subprocess
import re

from pathlib import Path


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('voidpackages')
    parser.add_argument('file')
    parser.add_argument('--branch', default='master')

    opts = parser.parse_args()

    pkgname = None
    version = None
    url = None

    with open(opts.file, 'r') as f:
        file_content = f.read()

    for line in file_content.splitlines():
        if line.startswith('pkgname'):
            pkgname = line.split('=', maxsplit=1)[1]
            #print(f'Name: {pkgname}')

        elif line.startswith('version'):
            version = line.split('=', maxsplit=1)[1]
            #print(f'Current version: {version}')

        elif line.startswith('homepage'):
            url = line.split('=', maxsplit=1)[1].lstrip('"').rstrip('"')
            #print(f'URL: {url}')

    assert pkgname
    assert version
    assert url

    src_dir = Path(opts.voidpackages) / 'hostdir' / 'sources'
    git_dir = src_dir / pkgname

    subprocess.run(['git', 'clone', url, pkgname], cwd=src_dir, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    subprocess.run(['git', 'reset', '--hard', f'origin/{opts.branch}'], cwd=git_dir, check=True, stdout=subprocess.DEVNULL)
    subprocess.run(['git', 'pull'], cwd=git_dir, check=True, stdout=subprocess.DEVNULL)
    subprocess.run(['git', 'submodule', 'update', '--init'], cwd=git_dir, check=True, stdout=subprocess.DEVNULL)

    try:
        result = subprocess.run(['git', 'describe', '--tags'], cwd=git_dir, capture_output=True, check=True)
        raw_new_version = result.stdout.decode().strip()
        new_version = re.sub(r'([^-]*-g)', r'r\1', raw_new_version)
        new_version = re.sub(r'^v(.*)', r'\1', new_version)
        new_version = new_version.replace('-', '.')

    except subprocess.CalledProcessError:
        result = subprocess.run(['git', 'rev-list', '--count', 'HEAD'], cwd=git_dir, capture_output=True, check=True)
        count = result.stdout.decode().strip()

        result = subprocess.run(['git', 'rev-parse', '--short', 'HEAD'], cwd=git_dir, capture_output=True, check=True)
        commit = result.stdout.decode().strip()

        new_version = f'r{count}.g{commit}'

    if new_version == version:
        return

    with open(opts.file, 'w') as f:
        f.truncate()

        for line in file_content.splitlines():
            if line.startswith('version'):
                f.write(f'version={new_version}\n')
            else:
                f.write(line)
                f.write('\n')

    print(f'{pkgname}: {version} -> {new_version}')
            

if __name__ == '__main__':
    sys.exit(main())

